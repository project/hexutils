<?php
namespace Drupal\hexutils\PDF;

use Drupal\hexutils\PDF\wPDF;
use Drupal\hexutils\Bootstrap\BootstrapBase;

class wPDFGenerator{

    protected $html;
    protected $htmlSnipp;
    protected $htmlFileName;
    protected $pdfFileName;
    protected $pageSize = 'A4';
    protected $fileName;
    protected $temporaryFolderLocation = 'public://wpdf/';
    protected $destination;
    protected $validPageSizes = ['A1','A2','A3','A4','A5','A6'];

    public function __construct($fileName = '', $destination = '', $config = []){
        $this->fileName = !empty($fileName) ? $fileName :'temp_'.rand(0,100).time();
        $this->setDestination($destination);
    }

    public function setConfiguration($config){
        if(!empty($config['page_size'])){
            $this->setPageSize($config['page_size']);
        }
    }

    public function setDestination($destination){
        $this->destination = !empty($destination) ? rtrim($destination,'/').'/' : $this->temporaryFolderLocation;
        // FileSystemInterface::prepareDirectory($this->destination, 
        //     FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
        file_prepare_directory($this->destination, 
            FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
    }

    public function setPageSize($pageSize){
        if(in_array($pageSize, $this->validPageSizes)){
            $this->pageSize = $pageSize;
        }
    }

    public function setHTML($html){
        $this->html = $html;
    }

    public function setStyleSheet($stylesheet, $media = 'screen,print'){
        $this->stylesheet = ['style'=>$stylesheet, 'media' => $media];
    }

    public function setHTMLSnipp($htmlSnipp, $force = false){
        if($force){
            $this->html = null;
        }
        $this->htmlSnipp = $htmlSnipp;
    }

    protected function prepareHTML(){
        if(empty($this->html)){

            \Drupal::messenger()->addError(t('Generating HTML'));
            $this->html = BootstrapBase::initiate('html',
                BootstrapBase::initiate('head',
                    BootstrapBase::initiate('style', $this->stylesheet['style'], ['media' =>  $this->stylesheet['media']])
                ).
                BootstrapBase::initiate('body',
                    $this->htmlSnipp
                )
            )->generate();
        }
    }

    protected function saveHTML(){
        $this->prepareHTML();
        $this->htmlFileName = $this->fileName.'.html';
        // $this->htmlFileName = FileSystemInterface::saveData($this->html, $this->destination.$this->htmlFileName, FileSystemInterface::EXISTS_REPLACE);
        $this->htmlFileName = file_unmanaged_save_data($this->html, $this->destination.$this->htmlFileName, FILE_EXISTS_REPLACE);
    }

    public function generate($extraParams = ''){
        $this->saveHTML();
        $this->pdfFileName = $this->fileName.'.pdf';
        if(file_exists($this->htmlFileName)){

            $pdf = new wPDF(
                drupal_realpath($this->htmlFileName),
                $this->fileName, 
                rtrim(drupal_realpath($this->destination),'/').'/');

            $pdf->binary('xvfb-run /usr/bin/wkhtmltopdf --page-size '.$this->pageSize.' '.$extraParams);
            // $pdf->setOptions(['orientation'=>'landscape']);
            $pdf->generatePDF();
            if(file_exists($this->destination.$this->pdfFileName)){
                return $this->destination.$this->pdfFileName;
            }
        }
        return false;
    }
}