<?php

namespace Drupal\hexutils\System;


class ShellCommand {
    public $escapeArgs = true;
    public $escapeCommand = false;
    public $useExec = false;
    public $captureStdErr = true;
    public $procCwd;
    public $procEnv;
    public $procOptions;
    public $locale;
    
    protected $_command;
    protected $_args = array();
    protected $_execCommand;
    protected $_stdOut = '';
    protected $_stdErr = '';
    protected $_exitCode;
    protected $_error = '';
    protected $_executed = false;
    
    public function __construct($options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        } elseif (is_string($options)) {
            $this->setCommand($options);
        }
    }
    
    public function setOptions($options) {
        foreach ($options as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = $value;
            } else {
                $method = 'set' . ucfirst($key);
                if (method_exists($this, $method)) {
                    call_user_func(array(
                        $this,
                        $method
                    ), $value);
                } else {
                    throw new \Exception("Unknown configuration option '$key'");
                }
            }
        }
        return $this;
    }
    
    public function setCommand($command) {
        if ($this->escapeCommand) {
            $command = escapeshellcmd($command);
        }
        if ($this->getIsWindows()) {
            // Make sure to switch to correct drive like "E:" first if we have a full path in command
            $chdrive = (isset($command[1]) && $command[1] === ':') ? $command[0] . ': && ' : '';
            $command = sprintf($chdrive . 'cd %s && %s', escapeshellarg(dirname($command)), basename($command));
        }
        $this->_command = $command;
        return $this;
    }
    
    public function getCommand() {
        return $this->_command;
    }
    
    public function getExecCommand() {
        if ($this->_execCommand === null) {
            $command = $this->getCommand();
            if (!$command) {
                $this->_error = 'Could not locate any executable command';
                return false;
            }
            $args = $this->getArgs();
            $this->_execCommand = $args ? $command . ' ' . $args : $command;
        }
        watchdog('command',$this->_execCommand);
        return $this->_execCommand;
    }
    
    public function setArgs($args) {
        $this->_args = array(
            $args
        );
        return $this;
    }
    
    public function getArgs() {
        return implode(' ', $this->_args);
    }
    
    public function addArg($key, $value = null, $escape = null) {
        $doEscape = $escape !== null ? $escape : $this->escapeArgs;
        $useLocale = $doEscape && $this->locale !== null;
        
        if ($useLocale) {
            $locale = setlocale(LC_CTYPE, 0); // Returns current locale setting
            setlocale(LC_CTYPE, $this->locale);
        }
        if ($value === null) {
            // Only escape single arguments if explicitely requested
            $this->_args[] = $escape ? escapeshellarg($key) : $key;
        } else {
            $separator = substr($key, -1) === '=' ? '' : ' ';
            if (is_array($value)) {
                $params = array();
                foreach ($value as $v) {
                    $params[] = $doEscape ? escapeshellarg($v) : $v;
                }
                $this->_args[] = $key . $separator . implode(' ', $params);
            } else {
                $this->_args[] = $key . $separator . ($doEscape ? escapeshellarg($value) : $value);
            }
        }
        if ($useLocale) {
            setlocale(LC_CTYPE, $locale);
        }
        
        return $this;
    }
    
    public function getOutput($trim = true) {
        return $trim ? trim($this->_stdOut) : $this->_stdOut;
    }
    
    public function getError($trim = true) {
        return $trim ? trim($this->_error) : $this->_error;
    }
    
    public function getStdErr($trim = true) {
        return $trim ? trim($this->_stdErr) : $this->_stdErr;
    }
    
    public function getExitCode() {
        return $this->_exitCode;
    }
    
    public function getExecuted() {
        return $this->_executed;
    }
    
    public function execute() {
        $command = $this->getExecCommand();
        
        if (!$command) {
            return false;
        }
        
        if ($this->useExec) {
            $execCommand = $this->captureStdErr ? "$command 2>&1" : $command;
            exec($execCommand, $output, $this->_exitCode);
            $this->_stdOut = implode("\n", $output);
            if ($this->_exitCode !== 0) {
                $this->_stdErr = $this->_stdOut;
                $this->_error = empty($this->_stdErr) ? 'Command failed' : $this->_stdErr;
                return false;
            }
        } else {
            $descriptors = array(
                1 => array(
                    'pipe',
                    'w'
                ),
                2 => array(
                    'pipe',
                    $this->getIsWindows() ? 'a' : 'w'
                )
            );
            $process = proc_open($command, $descriptors, $pipes, $this->procCwd, $this->procEnv, $this->procOptions);
            
            if (is_resource($process)) {
                
                $this->_stdOut = stream_get_contents($pipes[1]);
                $this->_stdErr = stream_get_contents($pipes[2]);
                fclose($pipes[1]);
                fclose($pipes[2]);
                
                $this->_exitCode = proc_close($process);
                
                if ($this->_exitCode !== 0) {
                    $this->_error = $this->_stdErr ? $this->_stdErr : "Failed without error message: $command";
                    return false;
                }
            } else {
                $this->_error = "Could not run command $command";
                return false;
            }
        }
        
        $this->_executed = true;
        
        return true;
    }
    
    public function getIsWindows() {
        return strncasecmp(PHP_OS, 'WIN', 3) === 0;
    }
    
    public function __toString() {
        return (string) $this->getExecCommand();
    }
}

