<?php
namespace Drupal\hexutils\FormElements;

Class DrupalFormNumber extends  DrupalInputElements{
    static public function initiate(){
        return new self;
    }
    
    public function step($step){
        $this->attributes(['step'=>$step]);
        return $this;
    }
    
    public function generate(&$form){
        if(!empty($this->title)){
            $form['#title'] = $this->title;
        }
        if(isset($this->required)){
            $form['#required'] = $this->required;
        }
        if(isset($this->description)){
            $form['#description'] = $this->description;
        }
        $this->type('number');
        parent::generate($form);
    }
}
