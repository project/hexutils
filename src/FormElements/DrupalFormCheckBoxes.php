<?php
namespace Drupal\hexutils\FormElements;

Class DrupalFormCheckBoxes extends  DrupalInputElements{
    protected $options;

    static public function initiate(){
        return new self;
    }

    public function options($options){
        $this->options = $options;
        return $this;
    }
    
    public function generate(&$form){
        if(!empty($this->title)){
            $form['#title'] = $this->title;
        }
        if(isset($this->required)){
            $form['#required'] = $this->required;
        }
        if(isset($this->description)){
            $form['#description'] = $this->description;
        }
        if(!empty($this->options)){
            $form['#options'] = $this->options;
        }
        $this->type('checkboxes');
        parent::generate($form);
    }
}
