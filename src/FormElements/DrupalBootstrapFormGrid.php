<?php
namespace Drupal\hexutils\FormElements;

use Drupal\Component\Utility\NestedArray;

class DrupalBootstrapFormGrid
{
    protected $grid = array();
    
    public function __construct()
    {
    }
    
    static public function initiate()
    {
        return new self();
    }
    
    public function row($key, $wrapper = 'div')
    {
        $this->grid[] = array(
            'key' => (array) $key,
            'wrapper' => $wrapper
        );
        return $this;
    }
    
    public function column($key, $width, $screen = 'md', $wrapper = 'div', $markup = '')
    {
        $this->grid[] = array(
            'key' => (array) $key,
            'width' => $width,
            'screen' => $screen,
            'wrapper' => $wrapper,
            'markup' => $markup
        );
        return $this;
    }
    
    public function generate(&$form)
    {
        foreach ($this->grid as $grid) {
            if (! empty($grid['key'])) {
                if (! empty($grid['width']) && ! empty($grid['screen'])) {
                    $class = 'col-' . $grid['screen'] . '-' . $grid['width'];
                } else {
                    $class = 'row';
                }
                $prefix = $grid['key'];
                $prefix[] = '#prefix';
                NestedArray::setValue($form, $prefix, '<' . $grid['wrapper'] . ' class="' . $class . '">', TRUE);
                $suffix = $grid['key'];
                $suffix[] = '#suffix';
                NestedArray::setValue($form, $suffix, '</' . $grid['wrapper'] . '>', TRUE);
                if (! empty($grid['markup'])) {
                    $markup = $grid['key'];
                    $markup[] = '#markup';
                    NestedArray::setValue($form, $markup, $grid['markup'], TRUE);
                }
            }
        }
    }
}