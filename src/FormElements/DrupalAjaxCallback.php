<?php
namespace Drupal\hexutils\FormElements;

Class DrupalAjaxCallback{
    protected $ajax;
    
    public function __construct($callback,$wrapper){
        $this->ajax['callback'] = $callback;
        $this->ajax['wrapper'] = $wrapper;
    }
    
    public function __call($name, $arguments){
        $validElements = array('callback','path','wrapper','effect','speed','event','prevent','method','progress');
        if(!empty($arguments[0]) && in_array($name, $validElements)){
            $this->ajax[$name] =  $arguments[0];
        } else {
            throw new Exception('Ajax Element not found');
        }
    }
    
    public function generate(){
        return $this->ajax;
    }
}