<?php
namespace Drupal\hexutils\FormElements;

Class DrupalFormSubmit extends DrupalButtonElements{
    protected $submit = array();
    protected $validate = array();
    
    static public function initiate(){
        return new self;
    }
    
    public function submit($submit){
        $this->submit = is_array($submit) ? $submit : array($submit);
        return $this;
    }
    
    public function validate($validate){
        $this->validate= is_array($validate) ? $validate: array($validate);
        return $this;
    }
    
    public function generate(&$form){
        $this->type('submit');
        parent::generate($form);
        if(!empty($this->submit)){
            $form['#submit'] = $this->submit;
        }
        if(!empty($this->validate)){
            $form['#validate'] = $this->validate;
        }
    }
}
