<?php

namespace Drupal\hexutils\FormElements;

Class DrupalForm{
    protected $type;
    protected $access;
    protected $ajax;
    protected $attributes;
    protected $default_value;
    protected $element_validate = [];
    
    public function type($type){
        $this->type= $type;
        return $this;
    }
    
    public function access($flag){
        $this->access = $flag;
        return $this;
    }
    
    public function default_value($default_value){
        $this->default_value= $default_value;
        return $this;
    }

    public function validate($element_validate){
        if(is_array($element_validate)){
            $this->element_validate = array_merge($this->element_validate, $element_validate);
        } else {
            $this->element_validate[] = $element_validate;
        }
        return $this;
    }
    
    public function ajax(DrupalAjaxCallback $ajax){
        $this->ajax = $ajax->generate();
        return $this;
    }
    
    public function attributes($attributes){
        $this->attributes = $attributes;
        return $this;
    }
    
    public function prefix($prefix){
        $this->prefix= $prefix;
        return $this;
    }
    
    public function suffix($suffix){
        $this->suffix= $suffix;
        return $this;
    }
    
    public function generate(&$form){
        if(!empty($this->type)){
            $form['#type'] = $this->type;
        }
        if(!empty($this->access)){
            $form['#access'] = $this->access;
        }
        if(!empty($this->ajax)){
            $form['#ajax'] = $this->ajax;
        }
        if(!empty($this->default_value)){
            $form['#default_value'] = $this->default_value;
        }
        if(!empty($this->element_validate)){
            $form['#element_validate'] = $this->element_validate;
        }
        if(!empty($this->attributes)){
            $form['#attributes'] = $this->attributes;
        }
        if(!empty($this->prefix)){
            $form['#prefix'] = $this->prefix;
        }
        if(!empty($this->suffix)){
            $form['#suffix'] = $this->suffix;
        }
    }
}
