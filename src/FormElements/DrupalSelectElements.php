<?php
namespace Drupal\hexutils\FormElements;

Class DrupalSelectElements extends DrupalInputElements{
    protected $options;
    protected $multiple;
    
    static public function initiate(){
        return new self;
    }
    
    public function options($options = array()){
        $this->options = $options;
        return $this;
    }
    
    public function multiple($flag = true){
        $this->multiple = $flag;
        return $this;
    }
    
    public function generate(&$form){
        if(empty($this->options)){
            $this->options = array();
        }
        $form['#options'] = $this->options;
        if(!empty($this->title)){
            $form['#title'] = $this->title;
        }
        if(isset($this->required)){
            $form['#required'] = $this->required;
        }
        if(isset($this->multiple)){
            $form['#multiple'] = $this->multiple;
        }
        if(isset($this->description)){
            $form['#description'] = $this->description;
        }
        $this->type('select');
        parent::generate($form);
    }
}