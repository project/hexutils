<?php
namespace Drupal\hexutils\FormElements;

Class DrupalButtonElements extends DrupalForm{
    protected  $title;
    protected  $required;
    
    public function value($value){
        $this->value = $value;
        return $this;
    }
    
    public function generate(&$form){
        if(!empty($this->value)){
            $form['#value'] = $this->value;
        }
        parent::generate($form);
    }
}
