<?php 

namespace Drupal\hexutils\FormElements;

Class DrupalManagedFile extends DrupalInputElements{
    protected $options;
    protected $multiple;
    protected $upload_validators = [];
    protected $upload_location;
    
    static public function initiate(){
        return new self;
    }
    
    public function file_validate_extensions($extensions){
        if(is_array($extensions)){
            $extensions = implode(' ',$extensions);
        }
        $this->upload_validators['file_validate_extensions'] = [$extensions];
        return $this;
    }
    
    public function file_validate_image_resolution($resolutions){
        if(is_array($resolutions)){
            $extensions = implode(' ',$resolutions);
        }
        $this->upload_validators['file_validate_image_resolution'] = [$resolutions];
        return $this;
    }
    
    public function file_validate_size($size){
        $this->upload_validators['file_validate_size'] = [$size];
        return $this;
    }
    
    public function upload_location($upload_location){
        $this->upload_location = $upload_location;
        return $this;
    }
    
    public function multiple($flag = true){
        $this->multiple = $flag;
        return $this;
    }
    
    public function generate(&$form){
        if(!empty($this->upload_location)){
            $form['#upload_location'] = $this->upload_location;
        }
        if(!empty($this->upload_validators)){
            $form['#upload_validators'] = $this->upload_validators;
        }
        if(!empty($this->title)){
            $form['#title'] = $this->title;
        }
        if(isset($this->required)){
            $form['#required'] = $this->required;
        }
        if(isset($this->multiple)){
            $form['#multiple'] = $this->multiple;
        }
        if(isset($this->description)){
            $form['#description'] = $this->description;
        }
        $this->type('managed_file');
        parent::generate($form);
    }
}