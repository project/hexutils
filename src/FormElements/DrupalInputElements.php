<?php
namespace Drupal\hexutils\FormElements;

Class DrupalInputElements extends DrupalForm{
    protected $required;
    protected $description;
    
    public function required($required = true){
        $this->required= $required;
        return $this;
    }
    
    public function description($description){
        $this->description= $description;
        return $this;
    }
    
    public function title($title){
        $this->title= $title;
        return $this;
    }
    
}