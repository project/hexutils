<?php

namespace  Drupal\hexutils\FormUtils;

use Drupal\Core\Form\FormStateInterface;

abstract Class AlertConfirm extends AjaxForm{
    
    public function AlertConfirmAjaxCallback(array &$form, FormStateInterface $form_state) {
        $output = isset($form['config']) ? $form['config'] : (isset($form['home']) ? $form['home'] : $form);
        return $output;
    }
    
    public function alertConfirmShow(array &$form, FormStateInterface $form_state){
        $storage = &$form_state->getStorage();
        $storage['tb_alert_show'] = true;
        $form_state->setStorage($storage);
        $form_state->setRebuild();
    }
    
    public function alertConfirmDecline(array &$form, FormStateInterface $form_state){
        $storage = &$form_state->getStorage();
        unset($storage['tb_alert_show']);
        $form_state->setStorage($storage);
        $form_state->setRebuild();
    }
}