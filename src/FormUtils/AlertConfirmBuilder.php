<?php
namespace  Drupal\hexutils\FormUtils;

use Drupal\Core\Form\FormStateInterface;
use Drupal\hexutils\FormUtils\AlertConfirm;
use Drupal\hexutils\FormElements\DrupalAjaxCallback;
use Drupal\hexutils\FormElements\DrupalFormSubmit;

Class AlertConfirmBuilder {
    protected $save_text = 'Save';
    protected $accept_text = 'Accept';
    protected $accept_functions = [];
    protected $accept_validate_functions = [];
    protected $decline_text = 'Decline';
    protected $decline_functions = [];
    protected $ajax_wrapper = 'ajax_replace_config_div';
    protected $ajax_callback= '::AlertConfirmAjaxCallback';
    protected $message= 'Are you sure ?';
    
    public function ajax_wrapper($wrapper = 'ajax_replace_config_div'){
        $this->ajax_wrapper= $wrapper;
    }
    
    public function ajax_callback($callback = '::alert_confirm_ajax_callback'){
        $this->ajax_callback = $callback;
    }
    
    public function save_text($text = 'Save'){
        $this->save_text = $text;
    }
    
    public function accept_text($text = 'Accept'){
        $this->accept_text = $text;
    }
    
    public function accept_functions($functions = []){
        $this->accept_functions= $functions;
    }
    
    public function accept_validate_functions($functions = []){
        $this->accept_validate_functions= $functions;
    }
    
    public function decline_text($text = 'Decline'){
        $this->decline_text = $text;
    }
    
    public function decline_functions($functions = []){
        $this->decline_functions= $functions;
    }
    
    public function alert_message($message){
        $this->message = $message;
    }
    
    public function BuildConfirm(array &$form, FormStateInterface $form_state){
        $ajaxElemet = new DrupalAjaxCallback($this->ajax_callback, $this->ajax_wrapper);
        $storage = &$form_state->getStorage();
        $this->accept_functions[] = '::alertConfirmDecline';
        $this->decline_functions[] = '::alertConfirmDecline';
        if (isset($storage['tb_alert_show']) && $storage['tb_alert_show'] == true) {
            $form['warning_body']['v2_warning'] = array(
                '#prefix' => '<div class="alert alert-block alert-warning">',
                '#markup' => '<b>'.$this->message.'</b>',
                '#suffix' => '</div>'
            );
            DrupalFormSubmit::initiate()->value($this->accept_text)->validate($this->accept_validate_functions)->submit($this->accept_functions)->ajax($ajaxElemet)->generate($form['accept_submit']);
            DrupalFormSubmit::initiate()->value($this->decline_text)->submit($this->decline_functions)->ajax($ajaxElemet)->generate($form['decline_submit']);
        } else {
            DrupalFormSubmit::initiate()->value($this->save_text)->submit('::alertConfirmShow')->ajax($ajaxElemet)->generate($form['show_warning_submit']);
        }
    }
}