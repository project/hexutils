<?php

namespace Drupal\hexutils\FormUtils;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

abstract Class AjaxForm extends FormBase{
    
    protected function clearFormInput(array $form, FormStateInterface $form_state) {
        // Replace the form entity with an empty instance.
//         $this->entity = $this->entityTypeManager->getStorage('my_entity_type')->create([]);
        // Clear user input.
        $input = $form_state->getUserInput();
        // We should not clear the system items from the user input.
        $clean_keys = $form_state->getCleanValueKeys();
        $clean_keys[] = 'ajax_page_state';
        foreach ($input as $key => $item) {
            if (!in_array($key, $clean_keys) && substr($key, 0, 1) !== '_') {
                unset($input[$key]);
            }
        }
        $form_state->setUserInput($input);
        // Rebuild the form state values.
        $form_state->setRebuild();
        $form_state->setStorage([]);
    }
    
}