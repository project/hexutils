<?php

namespace  Drupal\hexutils\FormUtils;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\hexutils\Traits\FormControllerBase;

abstract Class HexForm extends FormBase{
    use FormControllerBase;
}