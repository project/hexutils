<?php

namespace Drupal\hexutils\Bootstrap;

use Drupal\hexutils\Bootstrap\BootstrapBase;

Class Menu{
    private $id;
    private $menu = array();
    private $current_menu = array();
    private $current_menu_id = '';
    private $current_group = 'parent';
    private $config = array();

    public function __construct($id) {
        $this->id = $id;
    }

    public function config($config) {
        if (is_array($config)) {
            $this->config = $config;
        }
        return $this;
    }

    public function setGroup($group, $header = null, $weight = null) {
        if (!empty($this->current_menu)) {
            $this->menu[$this->current_group]['menu'][$this->current_menu_id] = $this->current_menu;
            $this->current_menu = NULL;
        }
        $this->current_group = $group;
        if (!isset($this->menu[$this->current_group])) {
            if (isset($header)) {
                $this->menu[$this->current_group]['header'] = $header;
            }
            if (isset($weight)) {
                $this->menu[$this->current_group]['weight'] = $weight;
            } else {
                $this->menu[$this->current_group]['weight'] = 9999999;
            }
        }
        return $this;
    }

    public function setParent($parentId) {
        if (!empty($this->current_menu)) {
            $this->menu[$this->current_group]['menu'][$this->current_menu_id] = $this->current_menu;
            $this->current_menu = NULL;
        }
        if (isset($this->menu[$this->current_group]['menu'][$parentId])) {
            $this->current_menu = $this->menu[$this->current_group]['menu'][$parentId];
            $this->current_menu_id = $parentId;
        }
        return $this;
    }

    public function addParentLink($id, $text, $href = "#", $icon = null, $label = null, $attributes = array()) {
        if (!empty($this->current_menu)) {
            $this->menu[$this->current_group]['menu'][$this->current_menu_id] = $this->current_menu;
            $this->current_menu = NULL;
        }
        $this->current_menu_id = $id;
        $this->current_menu = array(
            'value' => $text, 
            'href' => $href, 
            'attributes' =>array('text'=>$attributes), 
            'icon' => $icon, 
            'label' => $label
        );
        return $this;
    }

    public function attributes($attributes) {
        $this->current_menu['attributes']['core'] = $attributes;
        return $this;
    }

    public function addSubLink($id, $text, $href = "#", $icon = null, $label = null, $attributes = array(), $config = array()) {
        if (empty($this->current_menu['submenu'])) {
            $this->current_menu['submenu'] = self::initiate($this->id . '_submenu');
            $this->current_menu['submenu']->config($config);
        }
        $this->current_menu['submenu']->addParentLink($id, $text, $href, $icon, $label, $attributes);
        return $this;
    }

    static public function initiate($id) {
        return new self($id);
    }

    public function toggleParent($parentId, $groupId = null, $flag = true) {
        if (isset($groupId) && isset($this->menu[$groupId]['menu'][$parentId])) {
            $this->menu[$groupId]['menu'][$parentId]['hidden'] = $flag;
        } else {
            if (isset($this->menu[$this->current_group]['menu'][$parentId])) {
                if($parentId == $this->current_menu_id){
                    $this->current_menu['hidden'] = $flag;
                } else {
                    $this->menu[$this->current_group]['menu'][$parentId]['hidden'] = $flag;
                }
            }
        }
        return $this;
    }

    public function generate($hideAllGroupLabel = false) {
        $menus = array();
        
        $subMenuClass = isset($this->config['submenu']['class']) ? $this->config['submenu']['class'] : '';
        $subMenuPointerIcon = isset($this->config['submenu']['pointer']['icon']) ? $this->config['submenu']['pointer']['icon'] : '';
        $subMenuPointerClass = isset($this->config['submenu']['pointer']['class']) ? $this->config['submenu']['pointer']['class'] : '';
        $subMenuPointerWrapper = isset($this->config['submenu']['pointer']['wrapper']) ? $this->config['submenu']['pointer']['wrapper'] : '';
        $subMenuPointerLinkAttributes = isset($this->config['submenu']['pointer']['link']['attributes']) ? $this->config['submenu']['pointer']['link']['attributes'] : '';
        
        $iconClass = isset($this->config['icon']['class']) ? $this->config['icon']['class'] : '';
        $iconPrefix = isset($this->config['icon']['prefix']) ? $this->config['icon']['prefix'] : '';
        
        $parentClass = isset($this->config['parentClass']) ? $this->config['parentClass'] : '';
        
        $textWrapper = isset($this->config['text']['wrapper']) ? $this->config['text']['wrapper'] : '';
        $LinkClass = isset($this->config['link']['class']) ? $this->config['link']['class'] : array();
        
        $this->menu[$this->current_group]['menu'][$this->current_menu_id] = $this->current_menu;
        uasort( $this->menu, function($a, $b) {
            $weitage_a = isset($a['weight']) ? $a['weight'] : 0;
            $weitage_b = isset($b['weight']) ? $b['weight'] : 0;
            return $weitage_a > $weitage_b;
        });
        watchdog('menu', json_encode($this->menu));
        foreach ($this->menu as $group) {
            $hideGroup = false;
            $hidden = array();
            if (PHP_VERSION_ID >= 50500) {
                $hidden =  array_keys(array_column($group['menu'], 'hidden'), true);
            } else {
                foreach ($group['menu'] as $menu) {
                    if (isset($menu['hidden']) && $menu['hidden'] != true) {
                        $hidden[] = $menu['id'];
                    }
                }
            }
            if(count($group['menu'])-count($hidden) < 1){
                $hideGroup = true;
            }
            if(!$hideAllGroupLabel){
                if (isset($group['header']) && !$hideGroup) {
                    $menus[] = BootstrapBase::initiate('li', $group['header'], array(
                        'class' => 'header'
                    ))->generate();
                }
            }
            foreach ($group['menu'] as $menu) {
                if (!isset($menu['hidden']) || (isset($menu['hidden']) && $menu['hidden'] != true)) {
                    $iconHtml = '';
                    $subMenuIconContainter = '';
                    $subMenuPointerLink = '';
                    $submenus = '';
                    $attributes = isset($menu['attributes']['core']) ? $menu['attributes']['core'] : array();
                    if (isset($menu['submenu'])) {
                        $attributes['class'][] = $subMenuClass;
                        $submenus = $menu['submenu']->generate();
                        $subMenuIcon = BootstrapBase::initiate('i')->attributes(array(
                            'class' => array(
                                $iconClass, 
                                $iconPrefix . $subMenuPointerIcon
                            )
                        ))
                            ->generate();
                        $subMenuIconContainter = BootstrapBase::initiate($subMenuPointerWrapper, $subMenuIcon, array(
                            'class' => $subMenuPointerClass
                        ))->generate();
                        $subMenuPointerLink = BootstrapBase::initiate('a', $subMenuIconContainter, $subMenuPointerLinkAttributes)->generate();
                    }
                    if (isset($menu['icon'])) {
                        $iconHtml = BootstrapBase::initiate('i')->attributes(array(
                            'class' => array(
                                $iconClass, 
                                $iconPrefix . $menu['icon']
                            )
                        ))
                            ->generate();
                    }
                    if (!empty($textWrapper)) {
                        $text = BootstrapBase::initiate('span', $menu['value'])->generate();
                    } else {
                        $text = $menu['value'];
                    }
                    $menu['attributes']['text']['href'] = $menu['href'];
                    if(isset($menu['attributes']['text']['class'])){
                        if(is_array($menu['attributes']['text']['class'])){
                            $menu['attributes']['text']['class'][] = $LinkClass;
                        } else {
                        	$menu['attributes']['text']['class'] = (is_array($menu['attributes']['text']['class']) ? implode(' ',$menu['attributes']['text']['class']) : $menu['attributes']['text']['class'])
                        	.' '.(is_array($LinkClass) ? implode(' ',$LinkClass) : $LinkClass);
                        }
                    } else {
                        $menu['attributes']['text']['class'] = $LinkClass;
                    }
                    $textContainer = BootstrapBase::initiate('a', $iconHtml . $text . $subMenuPointerLink, $menu['attributes']['text'])->generate();
                    $menus[] = BootstrapBase::initiate('li', $textContainer.$submenus,$attributes)->generate();
                }
            }
        }
        return BootstrapBase::initiate('ul', implode('', $menus), array(
            'class' => $parentClass
        ))->generate();
    }
}