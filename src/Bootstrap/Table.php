<?php


namespace Drupal\hexutils\Bootstrap;

use Drupal\hexutils\Bootstrap\BootstrapBase;

Class Table{
    private $id;
    private $columns;
    private $show_header = true;
    private $current_columns;
    private $headers = array();
    private $attributes = array();
    private $data = array();

    public function __construct($id) {
        $this->id = $id;
    }
    
    static public function initiate($id) {
        return new self($id);
    }
    
    protected function updateFetch(){
        if (!empty($this->current_columns)) {
            $this->columns[$this->current_columns['key']] = $this->current_columns;
            $this->headers[$this->current_columns['key']] = $this->current_columns['key'];
            $this->current_columns = NULL;
        }
    }
    
    public function attributes($attributes) {
        $this->attributes = $attributes;
        return $this;
    }
    
    public function show_header($flag = true){
        $this->show_header = $flag;
        return $this;
    }

    public function column($key, $text='', $modifier = null, $attributes = array()){
        $this->updateFetch();
        $this->current_columns = array(
            'attributes' => $attributes,
            'key' => $key,
            'text' => $text,
            'modifier' => $modifier
        );
        return $this;
    }
    
    public function row($data, $attributes=[], $strict = false, $associative = false){
        $this->updateFetch();
        if(is_array($data)){
            $processed = array();
            if($strict){
                $diffcount = array_diff($this->headers, array_keys($data));
                if($diffcount == 0){
                    foreach ($this->headers as $key){
                        $processed[$key] = ['value' => $this->call_modifier($data, $key), 'attribute' => $attributes];
                    }
                }
            } else {
                if($associative){
                    foreach ($this->headers as $key){
                        $processed[$key] =  ['value' => $this->call_modifier($data, $key), 'attribute' => $attributes];
                    }
                } else {
                    $processed = array_fill_keys($this->headers, '');
                    foreach ($processed as $key => $value){
                        $processed[$key] =  ['value' => current($data), 'attribute' => $attributes] ;
                        next($data);
                    }
                }
            }
            $this->data[] = $processed;
        }
        return $this;
    }
    
    public function rows($data, $attributes = [], $strict = false, $associative = false){
        $this->updateFetch();
        if(is_array($data)){
            foreach ($data as $row){
                $this->row($row, $attributes, $strict, $associative);
            }
        }
        return $this;
    }
    
    private function call_modifier($data, $key){
        $intermediate_value = isset($data[$key]) ? $data[$key] : '';
        if(isset($this->columns[$key]['modifier'])){
            if(is_array($this->columns[$key]['modifier'])){
                $modifier_call = isset($this->columns[$key]['modifier']['call']) ? $this->columns[$key]['modifier']['call'] : (isset($this->columns[$key]['modifier'][0]) ? $this->columns[$key]['modifier'][0] : '');
                $params = $this->columns[$key]['modifier'];
                array_shift($params);
                foreach ($params as &$param){
                    if(substr( $k, 0, 1 ) == ':'){
                        $index = trim($k,':');
                        $param = isset($data[$index]) ? $data[$index] : $param;
                    }
                }
            } else {
                $modifier_call = isset($this->columns[$key]['modifier']) ? $this->columns[$key]['modifier'] : '';
                $params = array();
            }
            if(is_callable($modifier_call)){
                array_unshift($params, $intermediate_value);
                return call_user_func_array($modifier_call,$params);
            } else {
                if(!ENVIROMENT_PRODUCTION){
                    watchdog('Bootstrap Table', $modifier_call.' not found');
                }
                return $intermediate_value;
            }
        } else {
            return $intermediate_value;
        }
    }
    
    public function dataTable(){
        return $this->data;
    }

    public function __toString(){
        return $this->generate();
    }

    public function generate(){
        $this->updateFetch();
        $html = array();
        if($this->show_header){
            $header = array();
            foreach ($this->columns as $column) {
                $header[] = BootstrapBase::initiate('th', $column['text'], $column['attributes'])->generate();
            }
            $html[] = BootstrapBase::initiate('tr',implode('',$header))->generate();
        }
        foreach ($this->data as $row){
            $cells = array();
            foreach ($row as $cell){
                $cells[] = BootstrapBase::initiate('td', $cell['value'], $cell['attributes'])->generate();
            }
            $html[] = BootstrapBase::initiate('tr',implode('', $cells))->generate();
        }
        if(isset($this->attributes['class'])){
            if(is_array($this->attributes['class'])){
                $this->attributes['class'][] = 'table';
            } else {
                $this->attributes['class'] = 'table '.$this->attributes['class'];
            }
        } else {
            $this->attributes['class'] = 'table';
        }
        return BootstrapBase::initiate('table',implode('',$html),$this->attributes)->generate();
    }

}

