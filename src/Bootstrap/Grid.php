<?php

namespace Drupal\hexutils\Bootstrap;

use Drupal\hexutils\Bootstrap\BootstrapBase;

Class Grid{
    private $id;
    private $html;
    private $preprocessed_html = array();
    private $grid = array();
    private $current_element;
    private $breakPoints = array(
        'sm', 
        'md', 
        'xs', 
        'lg'
    );

    public function __construct($id) {
        $this->id = $id;
    }
    
    static public function initiate($id){
        return new self($id);
    }
    
    public function row($attributes = array(), $weight = 0) {
        if (!empty($this->current_element)) {
            $this->grid[] = $this->current_element;
            $this->current_element = NULL;
        }
        $this->current_element = array(
            'attributes' => $attributes, 
            'weight' => $weight, 
            'column' => array()
        );
        return $this;
    }

    public function attributes($attributes) {
        $this->current_element['attributes'] = $attributes;
        return $this;
    }

    public function weight($weight) {
        $this->current_element['weight'] = $weight;
        return $this;
    }

    public function column($value, $width, $offset = null, $attributes = array()) {
        if (is_array($width)) {
            foreach ($width as $screen => $value){
                $attributes['class'][] = 'col-'.$screen.'-'.$value;
            }
            // $width = array_unique($width);
            // $breakPoints = array_key_exists($breakPoints, $width);
            // if(empty($breakPoints)){
            // for($c=0;$c<count($width); $c++){
            
            // }
            // } else {
            
            // }
        } else {
            $attributes['class'][] = 'col-sm'.'-'.$width;
        }
        $this->current_element['column'][] = array(
            'value' => $value,
            'attributes'=>$attributes
        );
        return $this;
    }

    function generate() {
        $this->grid[] = $this->current_element;
        foreach ($this->grid as $grid) {
            $preprocessd_columns = array();
            $class = array();
            $class[] = 'row';
            foreach ($grid['column'] as $column) {
                $preprocessd_columns[] = BootstrapBase::initiate('div')->value($column['value'])
                    ->attributes($column['attributes'])
//                     ->mergerAttributes('class', $column_class)
                    ->generate();
            }
            $this->preprocessed_html[] = BootstrapBase::initiate('div')->attributes($grid['attributes'])
                ->mergerAttributes('class', $class)
                ->value(implode('', $preprocessd_columns))
                ->generate();
        }
        return implode('', $this->preprocessed_html);
    }
}