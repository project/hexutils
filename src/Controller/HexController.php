<?php
/**
 * @file
 * SublimiterAPI Controller
 */


namespace Drupal\hexutils\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\hexutils\Traits\FormControllerBase;

class HexController extends ControllerBase {
    use FormControllerBase;

    public function __construct(){
        $this->prepareVariables();
    }
    
}