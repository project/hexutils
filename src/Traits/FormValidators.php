<?php
/**
 * @file
 * Contains Registration Controller
 */
namespace Drupal\hexutils\Traits;

use \Drupal\Core\File\FileSystemInterface;

trait FormValidators {

    public function validateEmail($element, &$form_state){
        $email = $element['#value'];
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $form_state->setError($element, 'Email Address validation failed.');
        }
    }
    
}