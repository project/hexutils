<?php
/**
 * @file
 * Contains Registration Controller
 */
namespace Drupal\hexutils\Traits;

use \Drupal\Core\File\FileSystemInterface;

trait FormControllerBase {
    protected $identifier;
    protected $database;
    protected $session;
    protected $messenger;
    protected $logId;

    public function prepareVariables(){
        $this->database = \Drupal::database();
        $this->identifier = self::class;
        $this->session = \Drupal::request()->getSession();
        $this->messenger = \Drupal::messenger();
        $this->setLogId(static::class);
    }

    public function getDBConnection(){
        return $this->dbConnection;
    }

    public function setLogId($logId){
        $this->logId = $logId;
    }

    public function setLog($message, $level = 'notice'){
        \Drupal::logger($this->logId)->log($level, $message);
    }

    public function dbQuery($query, $params = [], $options = []){
        return $this->database->query($query, $params, $options);
    }

    public function dbQueryRange($query, $start, $limit, $params, $options = []){
        return $this->database->queryRange($query, $start, $limit, $params, $options);
    }

    public function dbSelect($table, $alias){
        return $this->database->select($table, $alias);
    }

    public function getSessionVar($key){
        return $this->session->get($key);
    }

    public function setSessionVar($key, $value){
        return $this->session->set($key, $value);
    }

    public function setMessage($message){
        $message = is_array($message) || is_object($message) ? print_r($message,1) : $message;
        $this->messenger->addMessage($message);
    }

    public function createAjaxModalLink($uri, $text, $class = ''){
        return Link::fromTextAndUrl(t($text),Url::fromUri($uri, [
            'attributes' => [
                'class' => 'use-ajax btn '.$class,
                'style' => 'font-decoration:none',
                'data-dialog-type' => 'modal'
            ]]))->toString();
    }
}